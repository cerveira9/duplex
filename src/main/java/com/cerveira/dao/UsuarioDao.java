/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerveira.dao;

import com.cerveira.model.Morador;
import com.cerveira.model.TipoUsuario;
import com.cerveira.model.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author fernandocerveira
 */
@Stateless
public class UsuarioDao {
    
    @PersistenceContext
    EntityManager em;
    
    public void gravar(Usuario usuario, Boolean alterar) throws Exception {
        if (alterar) {
            em.merge(usuario);
        } else {
            em.persist(usuario);
        }
        
    }
    
    public void remover(Usuario usuario) throws Exception {
        em.remove(em.merge(usuario));
    }
    
    
    public List<Usuario> getUsuarios() {
        Query q = em.createQuery("select u from Usuario u order by u.nomeUsuario");
        return q.getResultList();
    }
    
    //TIPOUSUARIO
    public void gravar(TipoUsuario tipoUsuario, Boolean alterar) throws Exception {
        if (alterar) {
            em.merge(tipoUsuario);
        } else {
            em.persist(tipoUsuario);
        }
        
    }
    
    public void remover(TipoUsuario tipoUsuario) throws Exception {
        em.remove(em.merge(tipoUsuario));
    }
    
    
    public List<TipoUsuario> getTipoUsuarios() {
        Query q = em.createQuery("select tu from TipoUsuario tu order by tu.nomeTipo");
        return q.getResultList();
    }
    
    public TipoUsuario getTipoUsuarioInquilinoAtivo(){
        Query q = em.createQuery("select tu from TipoUsuario tu where tu.idTipoUsuario = 2");
        return (TipoUsuario) q.getSingleResult();
    }
    
    //Morador
    public Morador getMoradorPorIdMorador(Morador morador){
        Query q = em.createQuery("select m from Morador m where m.idMorador = :id");
        q.setParameter("id", morador.getIdMorador());
        return (Morador) q.getSingleResult();
    }
    
    //Login
    public Usuario getUsuarioPorUsuarioESenha(String usuario, String senha){
        Query q = em.createQuery("select u from Usuario u where u.nomeUsuario = :usuario and u.senhaUsuario = :senha");
        q.setParameter("usuario", usuario);
        q.setParameter("senha", senha);
        return (Usuario) q.getSingleResult();
    }
}
