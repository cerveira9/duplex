/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerveira.dao;

import com.cerveira.model.Duplex;
import com.cerveira.model.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author fernandocerveira
 */
@Stateless
public class DuplexDao {
    
    @PersistenceContext
    EntityManager em;
    
    public void gravar(Duplex duplex, Boolean alterar) throws Exception {
        if (alterar) {
            em.merge(duplex);
        } else {
            em.persist(duplex);
        }
        
    }
    
    public void remover(Duplex duplex) throws Exception {
        em.remove(em.merge(duplex));
    }
    
    
    public List<Duplex> getDuplexes() {
        Query q = em.createQuery("select d from Duplex d order by d.numero");
        return q.getResultList();
    }
    
    public List<Duplex> getDuplexPorUsuario(Usuario usuario){
        Query q = em.createQuery("select d from Duplex d where d.idDuplex = :id");
        q.setParameter("id", usuario.getMorador().getDuplex().getIdDuplex());
        return q.getResultList();
    }
    
    public List<Duplex> getDuplexPorUsuarioCadastrante(Usuario usuario){
        Query q = em.createQuery("select d from Duplex d where d.idDuplex = :id");
        q.setParameter("id", usuario.getDuplex().getIdDuplex());
        return q.getResultList();
    }
}
