/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerveira.dao;

import com.cerveira.model.Morador;
import com.cerveira.model.Aluguel;
import com.cerveira.model.AluguelUpload;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author fernandocerveira
 */
@Stateless
public class AluguelDao {
    
    @PersistenceContext
    EntityManager em;
    
    public void gravar(Aluguel aluguel, Boolean alterar) throws Exception {
        if (alterar) {
            em.merge(aluguel);
        } else {
            em.persist(aluguel);
        }
        
    }
    
    public void remover(Aluguel aluguel) throws Exception {
        em.remove(em.merge(aluguel));
    }
    
    
    public List<Aluguel> getAlugueis() {
        Query q = em.createQuery("select a from Aluguel a order by a.dataVencimento");
        return q.getResultList();
    }
    
    public List<Aluguel> getAluguelPorMorador(Morador morador) {
        Query q = em.createQuery("select a from Aluguel a where a.morador.idMorador = :id order by a.dataVencimento");
        q.setParameter("id", morador.getIdMorador());
        return q.getResultList();
    }
    
    //AluguelUpload
    public List<AluguelUpload> getAluguelUploadPorAluguel(Aluguel aluguel){
        Query q = em.createQuery("select a from AluguelUpload a where a.aluguel.idAluguel = :id");
        q.setParameter("id", aluguel.getIdAluguel());
        return q.getResultList();
    }
    
    public void gravar(AluguelUpload aluguelUpload, Boolean alterar) throws Exception {
        if (alterar) {
            em.merge(aluguelUpload);
        } else {
            em.persist(aluguelUpload);
        }
        
    }
    
    public void remover(AluguelUpload aluguelUpload) throws Exception {
        em.remove(em.merge(aluguelUpload));
    }
}
