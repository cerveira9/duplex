/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerveira.dao;

import com.cerveira.model.CaucaoUpload;
import com.cerveira.model.ContratoUpload;
import com.cerveira.model.Duplex;
import com.cerveira.model.Morador;
import com.cerveira.model.Usuario;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author fernandocerveira
 */
@Stateless
public class MoradorDao {
    
    @PersistenceContext
    EntityManager em;
    
    public void gravar(Morador morador, Boolean alterar) throws Exception {
        if (alterar) {
            em.merge(morador);
        } else {
            em.persist(morador);
        }
        
    }
    
    public void remover(Morador morador) throws Exception {
        em.remove(em.merge(morador));
    }
    
    
    public List<Morador> getMoradores() {
        Query q = em.createQuery("select m from Morador m order by m.nome");
        return q.getResultList();
    }
    
    public List<Morador> getMoradoresPorDuplexAtivos(Duplex duplex) {
        Query q = em.createQuery("select m from Morador m where m.duplex.idDuplex = :id and m.ativo = true");
        q.setParameter("id", duplex.getIdDuplex());
        return q.getResultList();
    }
    
    public List<Morador> getMoradoresAtivos() {
        Query q = em.createQuery("select m from Morador m where m.ativo = true");
        return q.getResultList();
    }
    
    public List<Morador> getMoradoresInativos() {
        Query q = em.createQuery("select m from Morador m where m.ativo = false");
        return q.getResultList();
    }
    
    //ContratoUpload
    public List<ContratoUpload> getContratoUploadPorMorador(Morador morador){
        Query q = em.createQuery("select c from ContratoUpload c where c.morador.idMorador = :id");
        q.setParameter("id", morador.getIdMorador());
        return q.getResultList();
    }
    
    public void gravar(ContratoUpload contratoUpload, Boolean alterar) throws Exception {
        if (alterar) {
            em.merge(contratoUpload);
        } else {
            em.persist(contratoUpload);
        }
        
    }
    
    public void remover(ContratoUpload contratoUpload) throws Exception {
        em.remove(em.merge(contratoUpload));
    }
    
    //CaucaoUpload
    public List<CaucaoUpload> getCaucaoUploadPorMorador(Morador morador){
        Query q = em.createQuery("select c from CaucaoUpload c where c.morador.idMorador = :id");
        q.setParameter("id", morador.getIdMorador());
        return q.getResultList();
    }
    
    public void gravar(CaucaoUpload caucaoUpload, Boolean alterar) throws Exception {
        if (alterar) {
            em.merge(caucaoUpload);
        } else {
            em.persist(caucaoUpload);
        }
        
    }
    
    public void remover(CaucaoUpload caucaoUpload) throws Exception {
        em.remove(em.merge(caucaoUpload));
    }
    
    //MoradorLogado
    public List<Morador> getMoradorPorUsuario(Usuario usuario){
        Query q = em.createQuery("select m from Morador m where m.idMorador = :id");
        q.setParameter("id", usuario.getMorador().getIdMorador());
        return q.getResultList();
    }
}
