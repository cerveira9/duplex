/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerveira.dao;

import com.cerveira.model.ContaDeAgua;
import com.cerveira.model.Morador;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author fernandocerveira
 */
@Stateless
public class ContaDeAguaDao {
    
    @PersistenceContext
    EntityManager em;
    
    public void gravar(ContaDeAgua contaDeAgua, Boolean alterar) throws Exception {
        if (alterar) {
            em.merge(contaDeAgua);
        } else {
            em.persist(contaDeAgua);
        }
        
    }
    
    public void remover(ContaDeAgua contaDeAgua) throws Exception {
        em.remove(em.merge(contaDeAgua));
    }
    
    
    public List<ContaDeAgua> getContaDeAguas() {
        Query q = em.createQuery("select c from ContaDeAgua c order by c.idContaDeAgua");
        return q.getResultList();
    }
    
    public List<ContaDeAgua> getContaDeAguaPorMorador(Morador morador) {
        Query q = em.createQuery("select c from ContaDeAgua c where c.morador.idMorador = :id order by c.dataDoAviso");
        q.setParameter("id", morador.getIdMorador());
        return q.getResultList();
    }
}
