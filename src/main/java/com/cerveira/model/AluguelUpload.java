/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerveira.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author fernandocerveira
 */
@Entity
public class AluguelUpload implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idAluguelUpload;
    private int pagina;
    @Column(columnDefinition = "text")
    private String imagemAluguel;
    
    @ManyToOne
    private Aluguel aluguel;

    public Long getIdAluguelUpload() {
        return idAluguelUpload;
    }

    public void setIdAluguelUpload(Long idAluguelUpload) {
        this.idAluguelUpload = idAluguelUpload;
    }

    public int getPagina() {
        return pagina;
    }

    public void setPagina(int pagina) {
        this.pagina = pagina;
    }

    public String getImagemAluguel() {
        return imagemAluguel;
    }

    public void setImagemAluguel(String imagemAluguel) {
        this.imagemAluguel = imagemAluguel;
    }

    public Aluguel getAluguel() {
        return aluguel;
    }

    public void setAluguel(Aluguel aluguel) {
        this.aluguel = aluguel;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAluguelUpload != null ? idAluguelUpload.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AluguelUpload)) {
            return false;
        }
        AluguelUpload other = (AluguelUpload) object;
        if ((this.idAluguelUpload == null && other.idAluguelUpload != null) || (this.idAluguelUpload != null && !this.idAluguelUpload.equals(other.idAluguelUpload))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerveira.model.AluguelUpload[ id=" + idAluguelUpload + " ]";
    }
    
}
