/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerveira.model;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author fernandocerveira
 */
@Entity
public class ContaDeAgua implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idContaDeAgua;
    private double valor;
    private LocalDate dataDoAviso;
    private boolean pago;
    
    @ManyToOne
    private Morador morador;
    
    public Integer getIdContaDeAgua() {
        return idContaDeAgua;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public LocalDate getDataDoAviso() {
        return dataDoAviso;
    }

    public void setDataDoAviso(LocalDate dataDoAviso) {
        this.dataDoAviso = dataDoAviso;
    }

    public boolean isPago() {
        return pago;
    }

    public void setPago(boolean pago) {
        this.pago = pago;
    }

    public Morador getMorador() {
        return morador;
    }

    public void setMorador(Morador morador) {
        this.morador = morador;
    }

    public void setIdContaDeAgua(Integer idContaDeAgua) {
        this.idContaDeAgua = idContaDeAgua;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idContaDeAgua != null ? idContaDeAgua.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContaDeAgua)) {
            return false;
        }
        ContaDeAgua other = (ContaDeAgua) object;
        if ((this.idContaDeAgua == null && other.idContaDeAgua != null) || (this.idContaDeAgua != null && !this.idContaDeAgua.equals(other.idContaDeAgua))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerveira.model.ContaDeAgua[ id=" + idContaDeAgua + " ]";
    }
    
}
