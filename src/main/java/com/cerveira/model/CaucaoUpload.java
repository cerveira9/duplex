/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerveira.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author fernandocerveira
 */
@Entity
public class CaucaoUpload implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idCaucaoUpload;
    private int pagina;
    @Column(columnDefinition = "text")
    private String imagemCaucao;
    
    @ManyToOne
    private Morador morador;

    public Long getIdCaucaoUpload() {
        return idCaucaoUpload;
    }

    public void setIdCaucaoUpload(Long idCaucaoUpload) {
        this.idCaucaoUpload = idCaucaoUpload;
    }

    public int getPagina() {
        return pagina;
    }

    public void setPagina(int pagina) {
        this.pagina = pagina;
    }

    public String getImagemCaucao() {
        return imagemCaucao;
    }

    public void setImagemCaucao(String imagemCaucao) {
        this.imagemCaucao = imagemCaucao;
    }

    public Morador getMorador() {
        return morador;
    }

    public void setMorador(Morador morador) {
        this.morador = morador;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idCaucaoUpload != null ? idCaucaoUpload.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CaucaoUpload)) {
            return false;
        }
        CaucaoUpload other = (CaucaoUpload) object;
        if ((this.idCaucaoUpload == null && other.idCaucaoUpload != null) || (this.idCaucaoUpload != null && !this.idCaucaoUpload.equals(other.idCaucaoUpload))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerveira.model.CaucaoUpload[ id=" + idCaucaoUpload + " ]";
    }
    
}
