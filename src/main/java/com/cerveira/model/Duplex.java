/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerveira.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author fernandocerveira
 */
@Entity
public class Duplex implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idDuplex;
    private int numero;
    private String referenciaEnergia;
    private String endereco;
    
    public Integer getIdDuplex() {
        return idDuplex;
    }

    public void setIdDuplex(Integer idDuplex) {
        this.idDuplex = idDuplex;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getReferenciaEnergia() {
        return referenciaEnergia;
    }

    public void setReferenciaEnergia(String referenciaEnergia) {
        this.referenciaEnergia = referenciaEnergia;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDuplex != null ? idDuplex.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Duplex)) {
            return false;
        }
        Duplex other = (Duplex) object;
        if ((this.idDuplex == null && other.idDuplex != null) || (this.idDuplex != null && !this.idDuplex.equals(other.idDuplex))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerveira.model.Duplex1[ id=" + idDuplex + " ]";
    }
    
}
