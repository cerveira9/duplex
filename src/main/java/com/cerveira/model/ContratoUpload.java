/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerveira.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author fernandocerveira
 */
@Entity
public class ContratoUpload implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idContratoUpload;
    private int pagina;
    @Column(columnDefinition = "text")
    private String imagemContrato;
    
    @ManyToOne
    private Morador morador;

    public Long getId() {
        return idContratoUpload;
    }

    public void setId(Long idContratoUpload) {
        this.idContratoUpload = idContratoUpload;
    }

    public int getPagina() {
        return pagina;
    }

    public void setPagina(int pagina) {
        this.pagina = pagina;
    }

    public String getImagemContrato() {
        return imagemContrato;
    }

    public void setImagemContrato(String imagemContrato) {
        this.imagemContrato = imagemContrato;
    }

    public Morador getMorador() {
        return morador;
    }

    public void setMorador(Morador morador) {
        this.morador = morador;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idContratoUpload != null ? idContratoUpload.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContratoUpload)) {
            return false;
        }
        ContratoUpload other = (ContratoUpload) object;
        if ((this.idContratoUpload == null && other.idContratoUpload != null) || (this.idContratoUpload != null && !this.idContratoUpload.equals(other.idContratoUpload))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerveira.model.ContratoUpload[ id=" + idContratoUpload + " ]";
    }
    
}
