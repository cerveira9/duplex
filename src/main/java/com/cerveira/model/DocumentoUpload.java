/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerveira.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author fernandocerveira
 */
@Entity
public class DocumentoUpload implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idDocumentoUpload;
    private String documento;
    private int pagina;
    @Column(columnDefinition = "text")
    private String imagemDocumento;
    
    @ManyToOne
    private Morador morador;

    public Long getIdDocumentoUpload() {
        return idDocumentoUpload;
    }

    public void setIdDocumentoUpload(Long idDocumentoUpload) {
        this.idDocumentoUpload = idDocumentoUpload;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public int getPagina() {
        return pagina;
    }

    public void setPagina(int pagina) {
        this.pagina = pagina;
    }

    public String getImagemDocumento() {
        return imagemDocumento;
    }

    public void setImagemDocumento(String imagemDocumento) {
        this.imagemDocumento = imagemDocumento;
    }

    public Morador getMorador() {
        return morador;
    }

    public void setMorador(Morador morador) {
        this.morador = morador;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDocumentoUpload != null ? idDocumentoUpload.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the idDocumentoUpload fields are not set
        if (!(object instanceof DocumentoUpload)) {
            return false;
        }
        DocumentoUpload other = (DocumentoUpload) object;
        if ((this.idDocumentoUpload == null && other.idDocumentoUpload != null) || (this.idDocumentoUpload != null && !this.idDocumentoUpload.equals(other.idDocumentoUpload))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerveira.model.DocumentoUpload[ idDocumentoUpload=" + idDocumentoUpload + " ]";
    }
    
}
