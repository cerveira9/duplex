/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerveira.model;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 *
 * @author fernandocerveira
 */
@Entity
public class Morador implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idMorador;
    private String nome;
    private String nacionalidade;
    private String estadoCivil;
    private String profissao;
    private String genero;
    private String rg;
    private String orgaoExpeditor;
    private String cpf;
    private int vencimento;
    private int vencimentoMaisCinco;
    private String vencimentoPorExtenso;   
    private String vencimentoMaisCincoPorExtenso;    
    private double valorAluguel;
    private String valorAluguelPorExtenso;
    private double caucao;
    private String localDaCaucao;
    private LocalDate dataEntrada;
    private LocalDate dataFimDoContrato;
    private LocalDate dataSaida;
    private boolean ativo;
    private String email;
    private String contato;
    
    @OneToOne
    private Duplex duplex;

    public Integer getIdMorador() {
        return idMorador;
    }

    public void setIdMorador(Integer idMorador) {
        this.idMorador = idMorador;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome.toUpperCase();
    }

    public int getVencimento() {
        return vencimento;
    }

    public void setVencimento(int vencimento) {
        this.vencimento = vencimento;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil.toLowerCase();
    }

    public String getProfissao() {
        return profissao;
    }

    public void setProfissao(String profissao) {
        this.profissao = profissao.toLowerCase();
    }

    public double getCaucao() {
        return caucao;
    }

    public void setCaucao(double caucao) {
        this.caucao = caucao;
    }

    public String getLocalDaCaucao() {
        return localDaCaucao;
    }

    public void setLocalDaCaucao(String localDaCaucao) {
        this.localDaCaucao = localDaCaucao;
    }

    public LocalDate getDataEntrada() {
        return dataEntrada;
    }

    public void setDataEntrada(LocalDate dataEntrada) {
        this.dataEntrada = dataEntrada;
    }

    public LocalDate getDataSaida() {
        return dataSaida;
    }

    public void setDataSaida(LocalDate dataSaida) {
        this.dataSaida = dataSaida;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public Duplex getDuplex() {
        return duplex;
    }

    public void setDuplex(Duplex duplex) {
        this.duplex = duplex;
    }

    public String getNacionalidade() {
        return nacionalidade;
    }

    public void setNacionalidade(String nacionalidade) {
        this.nacionalidade = nacionalidade.toLowerCase();
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getOrgaoExpeditor() {
        return orgaoExpeditor;
    }

    public void setOrgaoExpeditor(String orgaoExpeditor) {
        this.orgaoExpeditor = orgaoExpeditor.toUpperCase();
    }

    public int getVencimentoMaisCinco() {
        return vencimentoMaisCinco;
    }

    public void setVencimentoMaisCinco(int vencimentoMaisCinco) {
        this.vencimentoMaisCinco = vencimentoMaisCinco;
    }

    public String getVencimentoPorExtenso() {
        return vencimentoPorExtenso;
    }

    public void setVencimentoPorExtenso(String vencimentoPorExtenso) {
        this.vencimentoPorExtenso = vencimentoPorExtenso;
    }

    public String getVencimentoMaisCincoPorExtenso() {
        return vencimentoMaisCincoPorExtenso;
    }

    public void setVencimentoMaisCincoPorExtenso(String vencimentoMaisCincoPorExtenso) {
        this.vencimentoMaisCincoPorExtenso = vencimentoMaisCincoPorExtenso;
    }

    public double getValorAluguel() {
        return valorAluguel;
    }

    public void setValorAluguel(double valorAluguel) {
        this.valorAluguel = valorAluguel;
    }

    public String getValorAluguelPorExtenso() {
        return valorAluguelPorExtenso;
    }

    public void setValorAluguelPorExtenso(String valorAluguelPorExtenso) {
        this.valorAluguelPorExtenso = valorAluguelPorExtenso.toUpperCase();
    }

    public LocalDate getDataFimDoContrato() {
        return dataFimDoContrato;
    }

    public void setDataFimDoContrato(LocalDate dataFimDoContrato) {
        this.dataFimDoContrato = dataFimDoContrato;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContato() {
        return contato;
    }

    public void setContato(String contato) {
        this.contato = contato;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMorador != null ? idMorador.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Morador)) {
            return false;
        }
        Morador other = (Morador) object;
        if ((this.idMorador == null && other.idMorador != null) || (this.idMorador != null && !this.idMorador.equals(other.idMorador))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.cerveira.model.Morador[ id=" + idMorador + " ]";
    }
    
}
