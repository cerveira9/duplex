/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerveira.gui;

import com.cerveira.dao.ContaDeAguaDao;
import com.cerveira.model.ContaDeAgua;
import com.cerveira.model.Morador;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author fernandocerveira
 */
@Named("guiContaDeAgua")
@SessionScoped
public class GuiContaDeAgua implements Serializable {

    private ContaDeAgua contaDeAgua;
    private List<ContaDeAgua> contaDeAguas;
    private Morador morador;
    private List<Morador> moradores;
    private boolean alterar;

    @EJB
    ContaDeAguaDao daoContaDeAgua;

    public ContaDeAgua getContaDeAgua() {
        return contaDeAgua;
    }

    public void setContaDeAgua(ContaDeAgua contaDeAgua) {
        this.contaDeAgua = contaDeAgua;
    }

    public List<ContaDeAgua> getContaDeAguas() {
        return contaDeAguas;
    }

    public void setContaDeAguas(List<ContaDeAgua> contaDeAguas) {
        this.contaDeAguas = contaDeAguas;
    }

    public Morador getMorador() {
        return morador;
    }

    public void setMorador(Morador morador) {
        this.morador = morador;
    }

    public List<Morador> getMoradores() {
        return moradores;
    }

    public void setMoradores(List<Morador> moradores) {
        this.moradores = moradores;
    }

    public boolean isAlterar() {
        return alterar;
    }

    public void setAlterar(boolean alterar) {
        this.alterar = alterar;
    }

    public String listaContaDeAguas() {
        contaDeAguas = daoContaDeAgua.getContaDeAguas();
        return "lstContaDeAguas";
    }
    
    public String cadastrarContaDeAgua(Morador morador) {
        this.morador = morador;
        contaDeAguas = daoContaDeAgua.getContaDeAguaPorMorador(morador);
        return "lstContaDeAguas";
    }

    public String novaContaDeAgua() {
        alterar = false;
        contaDeAgua = new ContaDeAgua();
        return "frmContaDeAgua";
    }

    public String gravarContaDeAgua() throws Exception {
        contaDeAgua.setMorador(morador);
        daoContaDeAgua.gravar(contaDeAgua, alterar);
        contaDeAguas = daoContaDeAgua.getContaDeAguaPorMorador(morador);
        return "lstContaDeAguas";
    }

    public String alterarContaDeAgua(ContaDeAgua contaDeAgua) {
        this.contaDeAgua = contaDeAgua;
        alterar = true;
        return "frmContaDeAgua";
    }

    public String removerContaDeAgua(ContaDeAgua contaDeAgua) throws Exception {
        daoContaDeAgua.remover(contaDeAgua);
        contaDeAguas = daoContaDeAgua.getContaDeAguaPorMorador(morador);
        return "lstContaDeAguas";
    }
    public String pagarContaDeAgua(ContaDeAgua contaDeAgua) throws Exception {
        this.contaDeAgua = contaDeAgua;
        alterar = true;
        contaDeAgua.setPago(true);
        daoContaDeAgua.gravar(contaDeAgua, alterar);
        return "lstContaDeAguas";
    }
}
