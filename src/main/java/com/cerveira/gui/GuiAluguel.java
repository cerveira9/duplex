/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerveira.gui;

import com.cerveira.dao.AluguelDao;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import com.cerveira.model.Aluguel;
import com.cerveira.model.AluguelUpload;
import com.cerveira.model.Morador;
import java.time.LocalDate;
import java.util.Base64;
import java.util.List;
import javax.ejb.EJB;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author fernandocerveira
 */
@Named("guiAluguel")
@SessionScoped
public class GuiAluguel implements Serializable {
    
    private Aluguel aluguel;
    private List<Aluguel> alugueis;
    private Morador morador;
    private List<Morador> moradores;
    private boolean alterar;
    private LocalDate hoje;
    private int dia;
    private String mes;
    private int ano;
    private int diaVencimento;
    private int mesVencimento;
    private int anoVencimento;
    private int diaAnterior;
    private int mesAnterior;
    private int anoAnterior;
    private AluguelUpload aluguelUpload;
    private List<AluguelUpload> aluguelUploads;
    
    @EJB
    AluguelDao daoAluguel;

    public Aluguel getAluguel() {
        return aluguel;
    }

    public void setAluguel(Aluguel aluguel) {
        this.aluguel = aluguel;
    }

    public List<Aluguel> getAlugueis() {
        return alugueis;
    }

    public void setAlugueis(List<Aluguel> alugueis) {
        this.alugueis = alugueis;
    }

    public Morador getMorador() {
        return morador;
    }

    public void setMorador(Morador morador) {
        this.morador = morador;
    }

    public List<Morador> getMoradores() {
        return moradores;
    }

    public void setMoradores(List<Morador> moradores) {
        this.moradores = moradores;
    }

    public boolean isAlterar() {
        return alterar;
    }

    public void setAlterar(boolean alterar) {
        this.alterar = alterar;
    }

    public LocalDate getHoje() {
        return hoje;
    }

    public void setHoje(LocalDate hoje) {
        this.hoje = hoje;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public int getDiaVencimento() {
        return diaVencimento;
    }

    public void setDiaVencimento(int diaVencimento) {
        this.diaVencimento = diaVencimento;
    }

    public int getMesVencimento() {
        return mesVencimento;
    }

    public void setMesVencimento(int mesVencimento) {
        this.mesVencimento = mesVencimento;
    }

    public int getAnoVencimento() {
        return anoVencimento;
    }

    public void setAnoVencimento(int anoVencimento) {
        this.anoVencimento = anoVencimento;
    }

    public int getDiaAnterior() {
        return diaAnterior;
    }

    public void setDiaAnterior(int diaAnterior) {
        this.diaAnterior = diaAnterior;
    }

    public int getMesAnterior() {
        return mesAnterior;
    }

    public void setMesAnterior(int mesAnterior) {
        this.mesAnterior = mesAnterior;
    }

    public int getAnoAnterior() {
        return anoAnterior;
    }

    public void setAnoAnterior(int anoAnterior) {
        this.anoAnterior = anoAnterior;
    }

    public AluguelUpload getAluguelUpload() {
        return aluguelUpload;
    }

    public void setAluguelUpload(AluguelUpload aluguelUpload) {
        this.aluguelUpload = aluguelUpload;
    }

    public List<AluguelUpload> getAluguelUploads() {
        return aluguelUploads;
    }

    public void setAluguelUploads(List<AluguelUpload> aluguelUploads) {
        this.aluguelUploads = aluguelUploads;
    }
    
    public String listaAlugueisPorMorador() {        
        alugueis = daoAluguel.getAluguelPorMorador(morador);
        return "lstAlugueis";
    }
    
    public String cadastrarAluguel(Morador morador) {
        this.morador = morador;
        alugueis = daoAluguel.getAluguelPorMorador(morador);
        return "lstAlugueis";
    }

    public String novoAluguel() {
        alterar = false;
        aluguel = new Aluguel();
        return "frmAluguel";
    }

    public String gravarAluguel() throws Exception {
        aluguel.setMorador(morador);
        daoAluguel.gravar(aluguel, alterar);
        alugueis = daoAluguel.getAluguelPorMorador(morador);
        return "lstAlugueis";
    }

    public String alterarAluguel(Aluguel aluguel) {
        this.aluguel = aluguel;
        alterar = true;
        return "frmAluguel";
    }

    public String removerAluguel(Aluguel aluguel) throws Exception {
        daoAluguel.remover(aluguel);
        alugueis = daoAluguel.getAluguelPorMorador(morador);
        return "lstAlugueis";
    }
    public String pagarAluguel(Aluguel aluguel) throws Exception {
        this.aluguel = aluguel;
        alterar = true;
        aluguel.setPago(true);
        daoAluguel.gravar(aluguel, alterar);
        return "lstAlugueis";
    }
           
    public String reciboAluguel(Aluguel aluguel) {
        this.aluguel = aluguel;
        hoje = LocalDate.now();
        dia = hoje.getDayOfMonth();
        nomeDoMes(hoje.getMonthValue());
        ano = hoje.getYear();
        System.out.println("data de hoje ---> " + dia + " de " + mes + " de " + ano);
        
        diaVencimento = aluguel.getDataVencimento().getDayOfMonth();
        mesVencimento = aluguel.getDataVencimento().getMonthValue();
        anoVencimento = aluguel.getDataVencimento().getYear();
        
        diaAnterior = aluguel.getDataVencimento().getDayOfMonth();
        mesAnterior = aluguel.getDataVencimento().getMonthValue() - 1;
        anoAnterior = aluguel.getDataVencimento().getYear();
        return "reciboAluguel";
    }

    public void nomeDoMes(int numeroMes) {
        switch (numeroMes) {
            case 1:
                this.mes = "Janeiro";
                break;
            case 2:
                this.mes = "Fevereiro";
                break;
            case 3:
                this.mes = "Março";
                break;
            case 4:
                this.mes = "Abril";
                break;
            case 5:
                this.mes = "Maio";
                break;
            case 6:
                this.mes = "Junho";
                break;
            case 7:
                this.mes = "Julho";
                break;
            case 8:
                this.mes = "Agosto";
                break;
            case 9:
                this.mes = "Setembro";
                break;
            case 10:
                this.mes = "Outubro";
                break;
            case 11:
                this.mes = "Novembro";
                break;
            case 12:
                this.mes = "Dezembro";
                break;
        }
    }
    
    //AluguelUpload
    public void handleFileUploadImagemAluguel(FileUploadEvent event) {
        this.aluguelUpload.setAluguel(aluguel);
        byte[] content = event.getFile().getContents();
        String resp = "data:image/jpeg;base64," + Base64.getEncoder().encodeToString(content);
        this.aluguelUpload.setImagemAluguel(resp);
        System.out.println("this.slider.setArteBase64(resp): " + resp);
        System.out.println("this.slider.getArteBase64(): " + this.aluguelUpload.getImagemAluguel());
    }
    
    public String cadastrarAluguelUpload(Aluguel aluguel) {
        this.aluguel = aluguel;
        aluguelUploads = daoAluguel.getAluguelUploadPorAluguel(aluguel);
        return "lstAluguelUploads";
    }
    
    public String listaAluguelUpload() {
        aluguelUploads = daoAluguel.getAluguelUploadPorAluguel(aluguel);
        return "lstAluguelUploads";
    }
    
    public String novoAluguelUpload() {
        alterar = false;
        aluguelUpload = new AluguelUpload();
        return "frmAluguelUpload";
    }
    
    public String gravarAluguelUpload() throws Exception {
        aluguelUpload.setAluguel(aluguel);
        daoAluguel.gravar(aluguelUpload, alterar);
        aluguelUploads = daoAluguel.getAluguelUploadPorAluguel(aluguel);
        return "lstAluguelUploads";
    }

    public String alterarAluguelUpload(AluguelUpload aluguelUpload) {
        this.aluguelUpload = aluguelUpload;
        alterar = true;
        return "frmAluguelUpload";
    }

    public String removerAluguelUpload(AluguelUpload aluguelUpload) throws Exception {
        daoAluguel.remover(aluguelUpload);
        aluguelUploads = daoAluguel.getAluguelUploadPorAluguel(aluguel);
        return "lstAluguelUploads";
    }
}
