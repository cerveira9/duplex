package com.cerveira.gui;

import com.cerveira.dao.DuplexDao;
import com.cerveira.model.Duplex;
import com.cerveira.model.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author fernandocerveira
 */
@Named("guiDuplex")
@SessionScoped
public class GuiDuplex implements Serializable {
    
    private Duplex duplex;
    private List<Duplex> duplexes;
    private boolean alterar;
    private Usuario usuario;
    private GuiLogin guiLogin;
    
    @EJB    
    DuplexDao daoDuplex;

    public Duplex getDuplex() {
        return duplex;
    }

    public void setDuplex(Duplex duplex) {
        this.duplex = duplex;
    }

    public List<Duplex> getDuplexes() {
        return duplexes;
    }

    public void setDuplexes(List<Duplex> duplexes) {
        this.duplexes = duplexes;
    }

    public boolean isAlterar() {
        return alterar;
    }

    public void setAlterar(boolean alterar) {
        this.alterar = alterar;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    public String listaDuplexes() {
        duplexes = daoDuplex.getDuplexes();                
        return "lstDuplexes";
    }

    public String novoDuplex() {
        alterar = false;
        duplex = new Duplex();
        return "frmDuplex";
    }

    public String gravarDuplex() throws Exception {
        daoDuplex.gravar(duplex, alterar);
        duplexes = daoDuplex.getDuplexes();
        return "lstDuplexes";
    }

    public String alterarDuplex(Duplex duplex) {
        this.duplex = duplex;
        alterar = true;
        return "frmDuplex";
    }

    public String removerDuplex(Duplex duplex) throws Exception {
        daoDuplex.remover(duplex);
        duplexes = daoDuplex.getDuplexes();
        return "lstDuplexes";
    }
    
    public String listaDuplexesPorUsuarioLogado(){
        System.out.println("PRÉ - usuario = guiLogin.getUsuario();" + usuario);
        usuario = guiLogin.logarUsuario();
        System.out.println("PÓS - usuario = guiLogin.getUsuario();" + usuario);
        duplexes = daoDuplex.getDuplexPorUsuario(usuario);
        return "lstDuplexes";
    }
}
