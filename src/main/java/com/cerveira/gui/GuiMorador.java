/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerveira.gui;

import com.cerveira.dao.MoradorDao;
import com.cerveira.dao.UsuarioDao;
import com.cerveira.model.CaucaoUpload;
import com.cerveira.model.ContratoUpload;
import com.cerveira.model.Duplex;
import com.cerveira.model.Morador;
import com.cerveira.model.Usuario;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.Base64;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author fernandocerveira
 */
@Named("guiMorador")
@SessionScoped
public class GuiMorador implements Serializable {

    private Morador morador;
    private List<Morador> moradores;
    private Duplex duplex;
    private boolean alterar;
    private boolean saida = false;
    private LocalDate hoje;
    private int dia;
    private String mes;
    private int ano;
    private int diaEntrada;
    private int mesEntrada;
    private int anoEntrada;
    private int diaFim;
    private int mesFim;
    private int anoFim;
    private String valorCaucao;
    private String valorCaucaoPorExtenso;
    private ContratoUpload contratoUpload;
    private List<ContratoUpload> contratoUploads;
    private CaucaoUpload caucaoUpload;
    private List<CaucaoUpload> caucaoUploads;
    private Usuario usuario;

    @EJB
    MoradorDao daoMorador;
    
    @EJB
    UsuarioDao daoUsuario;

    public Morador getMorador() {
        return morador;
    }

    public void setMorador(Morador morador) {
        this.morador = morador;
    }

    public List<Morador> getMoradores() {
        return moradores;
    }

    public void setMoradores(List<Morador> moradores) {
        this.moradores = moradores;
    }

    public Duplex getDuplex() {
        return duplex;
    }

    public void setDuplex(Duplex duplex) {
        this.duplex = duplex;
    }

    public boolean isAlterar() {
        return alterar;
    }

    public void setAlterar(boolean alterar) {
        this.alterar = alterar;
    }

    public boolean isSaida() {
        return saida;
    }

    public void setSaida(boolean saida) {
        this.saida = saida;
    }

    public LocalDate getHoje() {
        return hoje;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public void setHoje(LocalDate hoje) {
        this.hoje = hoje;
    }

    public int getDiaEntrada() {
        return diaEntrada;
    }

    public void setDiaEntrada(int diaEntrada) {
        this.diaEntrada = diaEntrada;
    }

    public int getMesEntrada() {
        return mesEntrada;
    }

    public void setMesEntrada(int mesEntrada) {
        this.mesEntrada = mesEntrada;
    }

    public int getAnoEntrada() {
        return anoEntrada;
    }

    public void setAnoEntrada(int anoEntrada) {
        this.anoEntrada = anoEntrada;
    }

    public int getDiaFim() {
        return diaFim;
    }

    public void setDiaFim(int diaFim) {
        this.diaFim = diaFim;
    }

    public int getMesFim() {
        return mesFim;
    }

    public void setMesFim(int mesFim) {
        this.mesFim = mesFim;
    }

    public int getAnoFim() {
        return anoFim;
    }

    public void setAnoFim(int anoFim) {
        this.anoFim = anoFim;
    }

    public String getValorCaucao() {
        return valorCaucao;
    }

    public void setValorCaucao(String valorCaucao) {
        this.valorCaucao = valorCaucao;
    }

    public String getValorCaucaoPorExtenso() {
        return valorCaucaoPorExtenso;
    }

    public void setValorCaucaoPorExtenso(String valorCaucaoPorExtenso) {
        this.valorCaucaoPorExtenso = valorCaucaoPorExtenso;
    }

    public ContratoUpload getContratoUpload() {
        return contratoUpload;
    }

    public void setContratoUpload(ContratoUpload contratoUpload) {
        this.contratoUpload = contratoUpload;
    }

    public List<ContratoUpload> getContratoUploads() {
        return contratoUploads;
    }

    public void setContratoUploads(List<ContratoUpload> contratoUploads) {
        this.contratoUploads = contratoUploads;
    }

    public CaucaoUpload getCaucaoUpload() {
        return caucaoUpload;
    }

    public void setCaucaoUpload(CaucaoUpload caucaoUpload) {
        this.caucaoUpload = caucaoUpload;
    }

    public List<CaucaoUpload> getCaucaoUploads() {
        return caucaoUploads;
    }

    public void setCaucaoUploads(List<CaucaoUpload> caucaoUploads) {
        this.caucaoUploads = caucaoUploads;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    //CRUD
    public String listaMoradores() {
        moradores = daoMorador.getMoradoresPorDuplexAtivos(duplex);
        return "lstMoradores";
    }

    public String cadastrarMorador(Duplex duplex) {
        this.duplex = duplex;
        moradores = daoMorador.getMoradoresPorDuplexAtivos(duplex);
        return "lstMoradores";
    }

    public String novoMorador() {
        alterar = false;
        morador = new Morador();
        return "frmMorador";
    }

    public void vencimentoFinalDoMes(int vencimento) {
        switch (vencimento) {
            case 26:
                morador.setVencimentoMaisCinco(1);
                break;
            case 27:
                morador.setVencimentoMaisCinco(2);
                break;
            case 28:
                morador.setVencimentoMaisCinco(3);
                break;
            case 29:
                morador.setVencimentoMaisCinco(4);
                break;
            case 30:
                morador.setVencimentoMaisCinco(5);
                break;
            case 31:
                morador.setVencimentoMaisCinco(6);
                break;
        }
    }

    public void vencimentoPorExtenso(int vencimento) {
        switch (vencimento) {
            case 1:
                morador.setVencimentoPorExtenso("UM");
                break;
            case 2:
                morador.setVencimentoPorExtenso("DOIS");
                break;
            case 3:
                morador.setVencimentoPorExtenso("TRÊS");
                break;
            case 4:
                morador.setVencimentoPorExtenso("QUATRO");
                break;
            case 5:
                morador.setVencimentoPorExtenso("CINCO");
                break;
            case 6:
                morador.setVencimentoPorExtenso("SEIS");
                break;
            case 7:
                morador.setVencimentoPorExtenso("SETE");
                break;
            case 8:
                morador.setVencimentoPorExtenso("OITO");
                break;
            case 9:
                morador.setVencimentoPorExtenso("NOVE");
                break;
            case 10:
                morador.setVencimentoPorExtenso("DEZ");
                break;
            case 11:
                morador.setVencimentoPorExtenso("ONZE");
                break;
            case 12:
                morador.setVencimentoPorExtenso("DOZE");
                break;
            case 13:
                morador.setVencimentoPorExtenso("TREZE");
                break;
            case 14:
                morador.setVencimentoPorExtenso("QUATORZE");
                break;
            case 15:
                morador.setVencimentoPorExtenso("QUINZE");
                break;
            case 16:
                morador.setVencimentoPorExtenso("DEZESSEIS");
                break;
            case 17:
                morador.setVencimentoPorExtenso("DEZESSETE");
                break;
            case 18:
                morador.setVencimentoPorExtenso("DEZOITO");
                break;
            case 19:
                morador.setVencimentoPorExtenso("DEZENOVE");
                break;
            case 20:
                morador.setVencimentoPorExtenso("VINTE");
                break;
            case 21:
                morador.setVencimentoPorExtenso("VINTE E UM");
                break;
            case 22:
                morador.setVencimentoPorExtenso("VINTE E DOIS");
                break;
            case 23:
                morador.setVencimentoPorExtenso("VINTE E TRÊS");
                break;
            case 24:
                morador.setVencimentoPorExtenso("VINTE E QUATRO");
                break;
            case 25:
                morador.setVencimentoPorExtenso("VINTE E CINCO");
                break;
            case 26:
                morador.setVencimentoPorExtenso("VINTE E SEIS");
                break;
            case 27:
                morador.setVencimentoPorExtenso("VINTE E SETE");
                break;
            case 28:
                morador.setVencimentoPorExtenso("VINTE E OITO");
                break;
            case 29:
                morador.setVencimentoPorExtenso("VINTE E NOVE");
                break;
            case 30:
                morador.setVencimentoPorExtenso("TRINTA");
                break;
        }
    }

    public void vencimentoMaisCincoPorExtenso(int vencimento) {
        switch (vencimento) {
            case 1:
                morador.setVencimentoMaisCincoPorExtenso("UM");
                break;
            case 2:
                morador.setVencimentoMaisCincoPorExtenso("DOIS");
                break;
            case 3:
                morador.setVencimentoMaisCincoPorExtenso("TRÊS");
                break;
            case 4:
                morador.setVencimentoMaisCincoPorExtenso("QUATRO");
                break;
            case 5:
                morador.setVencimentoMaisCincoPorExtenso("CINCO");
                break;
            case 6:
                morador.setVencimentoMaisCincoPorExtenso("SEIS");
                break;
            case 7:
                morador.setVencimentoMaisCincoPorExtenso("SETE");
                break;
            case 8:
                morador.setVencimentoMaisCincoPorExtenso("OITO");
                break;
            case 9:
                morador.setVencimentoMaisCincoPorExtenso("NOVE");
                break;
            case 10:
                morador.setVencimentoMaisCincoPorExtenso("DEZ");
                break;
            case 11:
                morador.setVencimentoMaisCincoPorExtenso("ONZE");
                break;
            case 12:
                morador.setVencimentoMaisCincoPorExtenso("DOZE");
                break;
            case 13:
                morador.setVencimentoMaisCincoPorExtenso("TREZE");
                break;
            case 14:
                morador.setVencimentoMaisCincoPorExtenso("QUATORZE");
                break;
            case 15:
                morador.setVencimentoMaisCincoPorExtenso("QUINZE");
                break;
            case 16:
                morador.setVencimentoMaisCincoPorExtenso("DEZESSEIS");
                break;
            case 17:
                morador.setVencimentoMaisCincoPorExtenso("DEZESSETE");
                break;
            case 18:
                morador.setVencimentoMaisCincoPorExtenso("DEZOITO");
                break;
            case 19:
                morador.setVencimentoMaisCincoPorExtenso("DEZENOVE");
                break;
            case 20:
                morador.setVencimentoMaisCincoPorExtenso("VINTE");
                break;
            case 21:
                morador.setVencimentoMaisCincoPorExtenso("VINTE E UM");
                break;
            case 22:
                morador.setVencimentoMaisCincoPorExtenso("VINTE E DOIS");
                break;
            case 23:
                morador.setVencimentoMaisCincoPorExtenso("VINTE E TRÊS");
                break;
            case 24:
                morador.setVencimentoMaisCincoPorExtenso("VINTE E QUATRO");
                break;
            case 25:
                morador.setVencimentoMaisCincoPorExtenso("VINTE E CINCO");
                break;
            case 26:
                morador.setVencimentoMaisCincoPorExtenso("VINTE E SEIS");
                break;
            case 27:
                morador.setVencimentoMaisCincoPorExtenso("VINTE E SETE");
                break;
            case 28:
                morador.setVencimentoMaisCincoPorExtenso("VINTE E OITO");
                break;
            case 29:
                morador.setVencimentoMaisCincoPorExtenso("VINTE E NOVE");
                break;
            case 30:
                morador.setVencimentoMaisCincoPorExtenso("TRINTA");
                break;
        }
    }

    public String gravarMorador() throws Exception {
        morador.setDuplex(duplex);
        vencimentoPorExtenso(morador.getVencimento());
        System.out.println("morador.getVencimentoPorExtenso() ----> " + morador.getVencimentoPorExtenso());
        if (morador.getVencimento() < 26) {
            morador.setVencimentoMaisCinco(morador.getVencimento() + 5);
        } else {
            vencimentoFinalDoMes(morador.getVencimento());
        }
        System.out.println("morador.getVencimentoMaisCinco() ----> " + morador.getVencimentoMaisCinco());
        vencimentoMaisCincoPorExtenso(morador.getVencimentoMaisCinco());
        System.out.println("morador.getVencimentoMaisCincoPorExtenso() ----> " + morador.getVencimentoMaisCincoPorExtenso());
        morador.setDataFimDoContrato(morador.getDataEntrada().plusMonths(30));
        System.out.println("morador.getDataFimDoContrato() ----> " + morador.getDataFimDoContrato());
        if (saida) {
            morador.setAtivo(false);
        } else {
            morador.setAtivo(true);
        }
        if (!alterar) {
            morador.setAtivo(true);
        }
        daoMorador.gravar(morador, alterar);
        moradores = daoMorador.getMoradoresPorDuplexAtivos(duplex);
        return "lstMoradores";
    }
    
    public String novoMoradorCadastrante(Duplex duplex) {
        this.duplex = duplex;
        System.out.println("Duplex Novo Morador Cadastrante ------>  " + this.duplex);
        alterar = false;
        morador = new Morador();
        return "frmMorador";
    }
    
    public String gravarMoradorCadastrante() throws Exception {
        morador.setDuplex(this.duplex);
        System.out.println("Duplex Novo Morador Cadastrante ------>  " + this.duplex);
        if (!alterar) {
            morador.setAtivo(true);
        }
        daoMorador.gravar(morador, alterar);
        moradores = daoMorador.getMoradoresPorDuplexAtivos(duplex);
        return "lstDuplex";
    }

    public String alterarMorador(Morador morador) {
        this.morador = morador;
        alterar = true;
        saida = false;
        return "frmMorador";
    }

    public String saidaMorador(Morador morador) {
        this.morador = morador;
        saida = true;
        alterar = true;
        return "frmSaidaMorador";
    }

    public String removerMorador(Morador morador) throws Exception {
        daoMorador.remover(morador);
        moradores = daoMorador.getMoradoresInativos();
        return "lstMoradoresInativos";
    }

    public String listaMoradoresAtivos() {
        moradores = daoMorador.getMoradoresAtivos();
        return "lstMoradoresAtivos";
    }

    public String listaMoradoresInativos() {
        moradores = daoMorador.getMoradoresInativos();
        return "lstMoradoresInativos";
    }

    public String contratoMorador(Morador morador) {
        this.morador = morador;
        hoje = LocalDate.now();
        dia = hoje.getDayOfMonth();
        nomeDoMes(hoje.getMonthValue());
        ano = hoje.getYear();
        System.out.println("data de hoje ---> " + dia + " de " + mes + " de " + ano);
        
        diaEntrada = morador.getDataEntrada().getDayOfMonth();
        mesEntrada = morador.getDataEntrada().getMonthValue();
        anoEntrada = morador.getDataEntrada().getYear();
        
        diaFim = morador.getDataFimDoContrato().getDayOfMonth();
        mesFim = morador.getDataFimDoContrato().getMonthValue();
        anoFim = morador.getDataFimDoContrato().getYear();
        return "contrato";
    }

    public void nomeDoMes(int numeroMes) {
        switch (numeroMes) {
            case 1:
                this.mes = "Janeiro";
                break;
            case 2:
                this.mes = "Fevereiro";
                break;
            case 3:
                this.mes = "Março";
                break;
            case 4:
                this.mes = "Abril";
                break;
            case 5:
                this.mes = "Maio";
                break;
            case 6:
                this.mes = "Junho";
                break;
            case 7:
                this.mes = "Julho";
                break;
            case 8:
                this.mes = "Agosto";
                break;
            case 9:
                this.mes = "Setembro";
                break;
            case 10:
                this.mes = "Outubro";
                break;
            case 11:
                this.mes = "Novembro";
                break;
            case 12:
                this.mes = "Dezembro";
                break;
        }
    }
    
    public String reciboCaucao (Morador morador) {
        this.morador = morador;
        hoje = LocalDate.now();
        dia = hoje.getDayOfMonth();
        nomeDoMes(hoje.getMonthValue());
        ano = hoje.getYear();
        return "reciboCaucao";
    }
    
    //ContratoUpload
    public void handleFileUploadImagemContrato(FileUploadEvent event) {
        this.contratoUpload.setMorador(morador);
        byte[] content = event.getFile().getContents();
        String resp = "data:image/jpeg;base64," + Base64.getEncoder().encodeToString(content);
        this.contratoUpload.setImagemContrato(resp);
        System.out.println("this.slider.setArteBase64(resp): " + resp);
        System.out.println("this.slider.getArteBase64(): " + this.contratoUpload.getImagemContrato());
    }
    
    public String cadastrarContratoUpload(Morador morador) {
        this.morador = morador;
        contratoUploads = daoMorador.getContratoUploadPorMorador(morador);
        return "lstContratoUploads";
    }
    
    public String listaContratoUpload() {
        contratoUploads = daoMorador.getContratoUploadPorMorador(morador);
        return "lstContratoUploads";
    }
    
    public String novoContratoUpload() {
        alterar = false;
        contratoUpload = new ContratoUpload();
        return "frmContratoUpload";
    }
    
    public String gravarContratoUpload() throws Exception {
        contratoUpload.setMorador(morador);
        daoMorador.gravar(contratoUpload, alterar);
        contratoUploads = daoMorador.getContratoUploadPorMorador(morador);
        return "lstContratoUploads";
    }

    public String alterarContratoUpload(ContratoUpload contratoUpload) {
        this.contratoUpload = contratoUpload;
        alterar = true;
        return "frmContratoUpload";
    }

    public String removerContratoUpload(ContratoUpload contratoUpload) throws Exception {
        daoMorador.remover(contratoUpload);
        contratoUploads = daoMorador.getContratoUploadPorMorador(morador);
        return "lstContratoUploads";
    }
    
    //CaucaoUpload
    public void handleFileUploadImagemCaucao(FileUploadEvent event) {
        this.caucaoUpload.setMorador(morador);
        byte[] content = event.getFile().getContents();
        String resp = "data:image/jpeg;base64," + Base64.getEncoder().encodeToString(content);
        this.caucaoUpload.setImagemCaucao(resp);
        System.out.println("this.slider.setArteBase64(resp): " + resp);
        System.out.println("this.slider.getArteBase64(): " + this.caucaoUpload.getImagemCaucao());
    }
    
    public String cadastrarCaucaoUpload(Morador morador) {
        this.morador = morador;
        caucaoUploads = daoMorador.getCaucaoUploadPorMorador(morador);
        return "lstCaucaoUploads";
    }
    
    public String listaCaucaoUpload() {
        caucaoUploads = daoMorador.getCaucaoUploadPorMorador(morador);
        return "lstCaucaoUploads";
    }
    
    public String novoCaucaoUpload() {
        alterar = false;
        caucaoUpload = new CaucaoUpload();
        return "frmCaucaoUpload";
    }
    
    public String gravarCaucaoUpload() throws Exception {
        caucaoUpload.setMorador(morador);
        daoMorador.gravar(caucaoUpload, alterar);
        caucaoUploads = daoMorador.getCaucaoUploadPorMorador(morador);
        return "lstCaucaoUploads";
    }

    public String alterarCaucaoUpload(CaucaoUpload caucaoUpload) {
        this.caucaoUpload = caucaoUpload;
        alterar = true;
        return "frmCaucaoUpload";
    }

    public String removerCaucaoUpload(CaucaoUpload caucaoUpload) throws Exception {
        daoMorador.remover(caucaoUpload);
        caucaoUploads = daoMorador.getCaucaoUploadPorMorador(morador);
        return "lstCaucaoUploads";
    }
    
}
