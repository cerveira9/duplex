/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerveira.gui;

import com.cerveira.dao.DuplexDao;
import com.cerveira.dao.MoradorDao;
import com.cerveira.dao.UsuarioDao;
import com.cerveira.model.Duplex;
import com.cerveira.model.Morador;
import com.cerveira.model.TipoUsuario;
import com.cerveira.model.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author fernandocerveira
 */
@Named("guiUsuario")
@SessionScoped
public class GuiUsuario implements Serializable {

    private Usuario usuario;
    private List<Usuario> usuarios;
    private boolean alterar;
    private Morador morador;
    private List<Morador> moradores;
    private TipoUsuario tipoUsuario;
    private List<TipoUsuario> tipoUsuarios;
    private Long idTipoUsuario;
    private Integer idMorador;
    private Integer idDuplex;
    private Duplex duplex;
    private List<Duplex> duplexes;

    @EJB
    UsuarioDao daoUsuario;
    
    @EJB
    MoradorDao daoMorador;
    
    @EJB
    DuplexDao daoDuplex;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public boolean isAlterar() {
        return alterar;
    }

    public void setAlterar(boolean alterar) {
        this.alterar = alterar;
    }

    public Morador getMorador() {
        return morador;
    }

    public void setMorador(Morador morador) {
        this.morador = morador;
    }

    public Integer getIdMorador() {
        return idMorador;
    }

    public void setIdMorador(Integer idMorador) {
        this.idMorador = idMorador;
    }

    public List<Morador> getMoradores() {
        return moradores;
    }

    public void setMoradores(List<Morador> moradores) {
        this.moradores = moradores;
    }

    public List<Morador> listaMoradores() {
        moradores = daoMorador.getMoradoresAtivos();
        return moradores;
    }

    private Morador getMoradorEscolhido() {

        for (Morador m : moradores) {
            if (m.getIdMorador().equals(idMorador)) {
                return m;
            }
        }
        return null;
    }

    public TipoUsuario getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(TipoUsuario tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public List<TipoUsuario> getTipoUsuarios() {
        return tipoUsuarios;
    }

    public void setTipoUsuarios(List<TipoUsuario> tipoUsuarios) {
        this.tipoUsuarios = tipoUsuarios;
    }

    public Long getIdTipoUsuario() {
        return idTipoUsuario;
    }

    public void setIdTipoUsuario(Long idTipoUsuario) {
        this.idTipoUsuario = idTipoUsuario;
    }

    public List<TipoUsuario> listaTipoUsuarios() {
        tipoUsuarios = daoUsuario.getTipoUsuarios();
        return tipoUsuarios;
    }

    private TipoUsuario getTipoUsuarioEscolhido() {

        for (TipoUsuario tp : tipoUsuarios) {
            if (tp.getIdTipoUsuario().equals(idTipoUsuario)) {
                return tp;
            }
        }
        return null;
    }

    public Integer getIdDuplex() {
        return idDuplex;
    }

    public void setIdDuplex(Integer idDuplex) {
        this.idDuplex = idDuplex;
    }

    public Duplex getDuplex() {
        return duplex;
    }

    public void setDuplex(Duplex duplex) {
        this.duplex = duplex;
    }

    public List<Duplex> getDuplexes() {
        return duplexes;
    }

    public void setDuplexes(List<Duplex> duplexes) {
        this.duplexes = duplexes;
    }
    
    public List<Duplex> listaDuplexes() {
        duplexes = daoDuplex.getDuplexes();
        return duplexes;
    }

    private Duplex getDuplexEscolhido() {

        for (Duplex d : duplexes) {
            if (d.getIdDuplex().equals(idDuplex)) {
                return d;
            }
        }
        return null;
    }
    
    //CRUD

    //USUARIO
    public String listaUsuario() {
        usuarios = daoUsuario.getUsuarios();
        return "lstUsuarios";
    }

    public String cadastrarUsuario(Morador morador) {
        this.morador = morador;
        usuarios = daoUsuario.getUsuarios();
        return "lstUsuarios";
    }

    public String novoUsuario() {
        alterar = false;
        usuario = new Usuario();
        return "frmUsuario";
    }

    public String gravarUsuario() throws Exception {
        usuario.setTipoUsuario(getTipoUsuarioEscolhido());  
        if (idTipoUsuario == 1){
            idMorador = null;
            idDuplex = null;
        }
        if (idTipoUsuario == 3) {
            idMorador = null;
            usuario.setDuplex(getDuplexEscolhido());
        }
        if (idMorador != null){
            usuario.setMorador(getMoradorEscolhido());
            usuario.setNomeUsuario(usuario.getMorador().getCpf());
            usuario.setSenhaUsuario(usuario.getMorador().getCpf());
        }
        daoUsuario.gravar(usuario, alterar);
        usuarios = daoUsuario.getUsuarios();
        return "lstUsuarios";
    }

    public String alterarUsuario(Usuario usuario) {
        this.usuario = usuario;
        alterar = true;
        return "frmUsuario";
    }

    public String removerUsuario(Usuario usuario) throws Exception {
        daoUsuario.remover(usuario);
        usuarios = daoUsuario.getUsuarios();
        return "lstUsuarios";
    }

    //TIPOUSUARIO
    public String novoTipoUsuario() {
        alterar = false;
        tipoUsuario = new TipoUsuario();
        return "frmTipoUsuario";
    }

    public String gravarTipoUsuario() throws Exception {
        daoUsuario.gravar(tipoUsuario, alterar);
        tipoUsuarios = daoUsuario.getTipoUsuarios();
        return "lstTipoUsuarios";
    }

    public String alterarTipoUsuario(TipoUsuario tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
        alterar = true;
        return "frmTipoUsuario";
    }

    public String removerTipoUsuario(TipoUsuario tipoUsuario) throws Exception {
        daoUsuario.remover(tipoUsuario);
        tipoUsuarios = daoUsuario.getTipoUsuarios();
        return "lstTipoUsuarios";
    }
    
    //IS
    public boolean isInquilinoAtivo() {
        if (getTipoUsuarioEscolhido() != null){
            if (idTipoUsuario == 2){
                return true;
            }
        }
        return false;
    }
    
    public boolean isCadastrante() {
        if (getTipoUsuarioEscolhido() != null){
            if (idTipoUsuario == 3){
                return true;
            }
        }
        return false;
    }
}
