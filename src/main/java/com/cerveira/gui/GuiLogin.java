/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cerveira.gui;

import com.cerveira.dao.DuplexDao;
import com.cerveira.dao.MoradorDao;
import com.cerveira.dao.UsuarioDao;
import com.cerveira.model.Duplex;
import com.cerveira.model.Morador;
import com.cerveira.model.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Named;

/**
 *
 * @author fernandocerveira
 */
@Named(value = "guiLogin")
@Stateless
public class GuiLogin implements Serializable {

    private Usuario usuario = null;
    private String senhaUsuario = null;
    private String nomeUsuario = null;
    private int tipoUsuario;
    private boolean logado = false;
    private boolean alterar;
    private List<Duplex> duplexes;
    private Duplex duplex;
    private List<Morador> moradores;
    private Morador morador;

    @EJB
    UsuarioDao daoUsuario;
    
    @EJB
    DuplexDao daoDuplex;
    
    @EJB
    MoradorDao daoMorador;

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getSenhaUsuario() {
        return senhaUsuario;
    }

    public void setSenhaUsuario(String senhaUsuario) {
        this.senhaUsuario = senhaUsuario;
    }

    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }

    public boolean isLogado() {
        return logado;
    }

    public void setLogado(boolean logado) {
        this.logado = logado;
    }

    public int getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(int tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    public List<Duplex> getDuplexes() {
        return duplexes;
    }

    public void setDuplexes(List<Duplex> duplexes) {
        this.duplexes = duplexes;
    }

    public Duplex getDuplex() {
        return duplex;
    }

    public void setDuplex(Duplex duplex) {
        this.duplex = duplex;
    }

    public List<Morador> getMoradores() {
        return moradores;
    }

    public void setMoradores(List<Morador> moradores) {
        this.moradores = moradores;
    }

    public Morador getMorador() {
        return morador;
    }

    public void setMorador(Morador morador) {
        this.morador = morador;
    }

    public boolean isAlterar() {
        return alterar;
    }

    public void setAlterar(boolean alterar) {
        this.alterar = alterar;
    }

    public Usuario logarUsuario() {
        usuario = daoUsuario.getUsuarioPorUsuarioESenha(nomeUsuario, senhaUsuario);
        System.out.println("usuario -----> " + usuario);
        return usuario;
    }

    public String isUsuarioLogado() {
        this.usuario = logarUsuario();
        if (usuario != null) {
            this.logado = true;
            this.tipoUsuario = usuario.getTipoUsuario().getIdTipoUsuario().intValue();
            return "index";
        } else {
            this.logado = false;
            return "login";
        }

    }

    public String loginPage() {
        return "login";
    }

    public String sair() {
        System.out.println("logado == " + logado);
        if (logado == true){
            logado = false;
            return "login";
        }
        return null;
    }
    
    public String listaDuplexesPorUsuarioLogado(){
        System.out.println("Usuario Para Duplex == " + usuario);
        duplexes = daoDuplex.getDuplexPorUsuario(usuario);
        System.out.println("Duplexes == " + duplexes);
        return "lstDuplexes";
    }
    
    public String listaDuplexesCadastrante() {
        System.out.println("Usuario Para Duplex == " + usuario);
        duplexes = daoDuplex.getDuplexPorUsuarioCadastrante(usuario);                
        return "lstDuplexes";
    }
    
    public String mostrarMorador(Duplex duplex) {
        this.duplex = duplex;
        System.out.println("Duplex para Morador == " + duplex);
        moradores = daoMorador.getMoradoresPorDuplexAtivos(duplex);
        System.out.println("Moradores == " + moradores);
        return "lstMoradores";
    }
    
    public String voltarMorador(){
        moradores = daoMorador.getMoradoresPorDuplexAtivos(duplex);
        System.out.println("Moradores == " + moradores);
        return "lstMoradores";
    }
    
    public String novoMorador() {
        alterar = false;
        morador = new Morador();
        return "frmMorador";
    }
    
    public String gravarMoradorCadastrante() throws Exception {
        morador.setDuplex(duplex);
        if (!alterar) {
            morador.setAtivo(true);
        }
        daoMorador.gravar(morador, alterar);
        moradores = daoMorador.getMoradoresPorDuplexAtivos(duplex);
        return "lstMoradores";
    }
}
