/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.spectral.utils;

import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;


public class JSFUtil {

    public static void mostrarMensagem(String mensagem) {
        FacesContext context = FacesContext.getCurrentInstance();
        FacesMessage message = new FacesMessage();
        message.setDetail(mensagem);
        message.setSummary("");
        context.addMessage(null, message);
    }
    
     public static void mostrarMensagemValoresNaoInformados() {
        FacesContext context = FacesContext.getCurrentInstance();
        FacesMessage message = new FacesMessage();
        message.setDetail(
                "Favor preencher todos os campos"
        );
        message.setSummary("");
        context.addMessage(null, message);
    }

    public static String getRequestParameter(String parametro) {
        FacesContext context = FacesContext.getCurrentInstance();
        Map<String, String> requestParams = context.getExternalContext().getRequestParameterMap();
        return requestParams.get(parametro);
    }


    public static ExternalContext getExternalContext() {
        FacesContext fcontext = FacesContext.getCurrentInstance();
        return fcontext.getExternalContext();

    }


    public static String getPathUpload() {
        FacesContext aFacesContext = FacesContext.getCurrentInstance();
        ServletContext context = (ServletContext) aFacesContext.getExternalContext().getContext();
        String realPath = context.getRealPath("/");
        return realPath;
    }

}
